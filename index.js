const express = require("express");
const fs = require("fs").promises;
const app = express();
const cors = require("cors");
const path = require("path");
app.use(cors());
app.use(express.json());

app.use((req, res, next) => {
  console.log(req.method, req.originalUrl);

  next();
});

app.post("/api/files", async (req, res) => {
  const { filename, content } = req.body;
  await fs.mkdir(path.join(__dirname, "files"), { recursive: true }, (err) => {
    res.json({ message: "Already exists" });
  });

  if (!filename || !content) {
    res.json({ message: "Please specify 'content' parameter" });
  }
  await fs.writeFile(`./files/${filename}`, content, (err) => {
    res.status(400).json({ message: "Server error" });
  });

  res.status(200).json({ message: "File created successfully" });
});

app.get("/api/files", async (req, res) => {
  try {
    const directoryPath = path.join(__dirname, "files");
    const filesName = await fs.readdir(directoryPath, (err) => {
      res.status(400).json({ message: "Client error" });
    });
    res.json({ message: "Success", files: filesName });
  } catch (err) {
    console.log(err);
  }
});

app.get("/api/files/:filename", async (req, res) => {
  try {
    const filename = req.params.filename;
    const directoryPath = path.join(__dirname, "files");
    const filesName = await fs.readdir(directoryPath, (err) => {
      res.status(400).json({ message: "Client error" });
    });
    const searchedFile = filesName.filter((item) => (item = item == filename));
    console.log(searchedFile);
    const extensionFile = searchedFile[0].split(".");
    const contentFile = await fs.readFile(
      `./files/${searchedFile[0]}`,
      "utf-8"
    );
    const createdDate = fs.stat(`./files/${searchedFile[0]}`);
    res.json({
      message: "Success",
      filename: searchedFile[0],
      content: contentFile,
      extension: extensionFile[1],
      uploadedDate: (await createdDate).birthtime,
    });
  } catch (err) {
    res.send(err);
  }
});

// app.post("/api/files", (req, res) => {
//   console.log(req.body);
//   if (!req.body.content) {
//     res.status(400).send({ message: "Please specify 'content' parameter" });
//   } else {
//     file.push(req.body);
//     console.log(file);
//     fs.writeFile(
//       `./files/${req.body.filename}`,
//       req.body.content.toString(),
//       "utf8",
//       () => {
//         res.status(200).send({ message: "File created successfully" });
//       }
//     );
//   }
// });

// app.use((err, req, res, next) => {
//   if (err) {
//     res.status(400).json({
//       message: "Client error",
//     });
//   }

//   next();
// });

// app.get("/api/files", (req, res) => {
//   res.json({
//     message: "Success",
//     files: file.map((item) => item.filename),
//   });
// });

// app.get("/api/files/:fileName", async (req, res) => {
//   try {
//     const data = await fs.readFile(`./files/${req.params.fileName}`, "utf8");
//     const splitterArr = req.params.fileName.split(".");
//     res.json({
//       message: "Success",
//       filename: req.params.fileName,
//       content: data,
//       extension: splitterArr[1],
//       uploadedDate: new Date(Date.now()),
//     });
//   } catch (err) {
//     res.status(400).json({
//       message: `No file with '${req.params.fileName}' filename found`,
//     });
//   }
// });

// app.listen(8080, () => {
//   console.log("Server is running");
// });

// function repeatedStr(str) {
//   let data = str
//     .split("")
//     .filter((item, i, arr) => {
//       return arr.indexOf(item) === i;
//     })
//     .join("");

//   return data;
// }

app.listen(8080, () => {
  console.log("server is running");
});
